export const HOME = 'navigation/HOME';
export const PROFILE = 'navigation/PROFILE';

export const goGome = () => ({ type: HOME });
export const goProfile = () => ({ type: PROFILE });