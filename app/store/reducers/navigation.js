import { createReducer } from '../createReducer';

import {
  HOME,
  PROFILE
} from '../actions/navigation';

export const navigationReducer = createReducer({ screen: 'HOME' }, {
  [HOME]: state => Object.assign({}, state, { screen: 'HOME' }),
  [PROFILE]: state => Object.assign({}, state, { screen: 'PROFILE' }),
});