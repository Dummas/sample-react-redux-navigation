import React from 'react';
import { View } from 'react-native';
import { Header } from '../header';
import { Container } from '../container';

export const Navigation = () => (
  <View style={{
    flex: 1,
    flexDirection: 'column',
    marginTop: 80,
  }}>
    <Header />
    <Container />
  </View>
);