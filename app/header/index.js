import React from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet } from 'react-native';
import { getScreen } from '../store/selectors/navigation';

const HeaderComponent = ({ screen }) => (
  <View style={{
    alignItems: 'center',
    marginBottom: 20
  }}>
    <Text>Header {screen}</Text>
  </View>
);

const mapStateToProps = (state) => ({
  screen: getScreen(state),
})

export const Header = connect(
  mapStateToProps,
)(HeaderComponent);