import React from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableHighlight, StyleSheet } from 'react-native';
import { getScreen } from '../store/selectors/navigation';
import { goGome, goProfile } from '../store/actions/navigation';

const ContainerComponent = ({ 
  goHome,
  goProfile,
}) => (
  <View>
    <Text>Container stays the same</Text>
    <View style={{
      flexDirection: 'row'
    }}>
      <TouchableHighlight style={[ style.button ]} onPress={goHome}>
        <Text>Go home</Text>
      </TouchableHighlight>
      <TouchableHighlight style={[ style.button ]} onPress={goProfile}>
        <Text>Go profile</Text>
      </TouchableHighlight>
    </View>
    
  </View>
)

const mapStateToProps = (state) => ({
  screen: getScreen(state),
});

const mapDispatchToProps = (dispatch) => ({
  goHome: () => dispatch(goGome()),
  goProfile: () => dispatch(goProfile())
});

export const Container = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ContainerComponent);

const style = StyleSheet.create({
  button: {
    borderWidth: 1,
    borderRadius: 6,
    marginVertical: 10,
    marginHorizontal: 10,
    height: 100,
    alignItems: 'center',
    flex: 1
  }
});